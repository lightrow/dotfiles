#!/bin/bash

WIDTH=25
START=0
END=$WIDTH
START_SND=0
END_SND=1

for (( ; ; ))
do	
	FULL_LINE="$(playerctl metadata artist) - $(playerctl metadata title) /// "
	if [[ "$PREV_LINE" != "$FULL_LINE" ]]; then
		START=0
		END=$WIDTH
		START_SND=0
		END_SND=1
	fi
	LEN=$(echo "$FULL_LINE" | wc -m)
	LEN=$(($LEN-1))
	if [[ "$LEN"-5 -gt "$WIDTH" ]]; then
		NEW_LINE=$(echo "${FULL_LINE:$START:$END}")
		if [[ "$START" -gt "$LEN"-25 ]]; then
			NEW_LINE=$NEW_LINE$(echo "${FULL_LINE:$START_SND:$END_SND}")
			END_SND=$(($END_SND+1))
			START=$(($START+1))
			#END=$(($END+1))
			if [[ "$END_SND" -eq "$WIDTH" ]]; then
				START=0
				END=$WIDTH
				END_SND=1
			fi
		else
			START=$(($START+1))
			#END=$(($END+1))
		fi
		echo "$NEW_LINE"
	else
		echo "$(playerctl metadata artist) - $(playerctl metadata title)"
	fi
	PREV_LINE="$FULL_LINE"
	sleep 0.5
done
